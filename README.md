## Instructions
* Make sure your gcloud do have correct permissions to create APP ENGINE resources, if not you can login with your user account, otherwise ignore this step
    * gcloud auth login
    * gcloud config set project project_id
* Get the code
    * git clone https://gitlab.com/synechron-elevate1/appengine
    * cd appengine
* Deploy the code on APP ENGINE
    * gcloud app deploy
